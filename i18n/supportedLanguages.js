export default {
  name: 'supportedLanguages',
  title: "Languages",
  type: 'document',
  fields: [
    {
      type: 'array',
      name: 'languages',
      of: [{type: 'language'}]
    },
  ],
  preview: {
    select: {
      languages: 'languages'
    },
    prepare({languages}) {
      return {
        title: languages.map(lang => {
          return lang.active === true ? lang.code : '*' + lang.code
        }).join(', '),
      }
    }
  }
}
