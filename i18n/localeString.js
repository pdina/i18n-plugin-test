import client from 'part:@sanity/base/client'

const query = `
  *[_type == "supportedLanguages"][0]{languages}
`
async function getSupportedLanguages() {
  return await client.fetch(query).then(
    (result) => {
      return result.languages
    }).then((languages) => {
      const res = languages.map((lang) => ({
         type: 'string',
         name: lang.code,
         title: lang.name,
      }))
      return res
    })
  }

export default {
  name: 'localeString',
  type: 'object',
  title: 'Localized string',
  fieldsets: [
    {
      title: 'Translations',
      name: 'translations',
      options: {collapsible: true}
    }
  ],
  fields: getSupportedLanguages().then((res) => {
    console.log(res)
    return res
  }),
}
