  export default {
    name: 'language',
    title: 'Language',
    type: 'object',
    fields: [
      {
        title: 'Active',
        name: 'active',
        type: 'boolean',
        options: {
          layout: 'checkbox'
        }
      },
      {
        title: 'ISO Code',
        description: 'https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes',
        name: 'code',
        type: 'string',
      },
      {
        title: 'Title',
        description: 'Verbose name, eg. italian, english, etc',
        name: 'title',
        type: 'string',
      },
    ]
  }
